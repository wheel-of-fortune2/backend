package cz.titanus.wheeloffortuneapi.user;

import cz.titanus.wheeloffortuneapi.exceptions.ResourceAlreadyExistsException;
import cz.titanus.wheeloffortuneapi.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User with username %s was not found", username)));
        return new SimpleUserDetails(user);
    }

    public User registerUser(User user) {
        userRepository.findByUsername(user.getUsername()).ifPresent(foundUser -> {
            throw new ResourceAlreadyExistsException(String.format("User with username \"%s\" already exists", foundUser.getUsername()));
        });
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    public User getUserById(Long id) {
        return userRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException(String.format("User with id %d was not found", id))
        );
    }
}
