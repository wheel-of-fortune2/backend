package cz.titanus.wheeloffortuneapi.wheel;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import cz.titanus.wheeloffortuneapi.user.User;
import cz.titanus.wheeloffortuneapi.wheelPart.WheelPart;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "wheel")
public class Wheel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wheel_generator")
    @SequenceGenerator(name = "wheel_generator", sequenceName = "wheel_sequence", allocationSize = 50)
    private Long id;
    private String name;

    @JsonManagedReference
    @OneToMany(mappedBy = "wheel", cascade = CascadeType.ALL)
    private List<WheelPart> parts;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    protected Wheel() {
    }

    public Wheel(Long id, String name, List<WheelPart> parts) {
        this.id = id;
        this.name = name;
        this.parts = parts;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<WheelPart> getParts() {
        return parts;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setParts(List<WheelPart> parts) {
        this.parts = parts;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
