package cz.titanus.wheeloffortuneapi.wheel;

import cz.titanus.wheeloffortuneapi.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WheelRepository extends JpaRepository<Wheel, Long> {
    List<Wheel> findByUserId(Long userId);

    Optional<Wheel> findByIdAndUserId(Long id, Long userId);

    Long deleteByIdAndUserId(Long id, Long userId);
}
