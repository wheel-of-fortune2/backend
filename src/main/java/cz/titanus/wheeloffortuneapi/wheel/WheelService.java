package cz.titanus.wheeloffortuneapi.wheel;

import cz.titanus.wheeloffortuneapi.exceptions.CustomException;
import cz.titanus.wheeloffortuneapi.exceptions.ResourceNotFoundException;
import cz.titanus.wheeloffortuneapi.user.User;
import cz.titanus.wheeloffortuneapi.wheelPart.WheelPart;
import cz.titanus.wheeloffortuneapi.wheelPart.WheelPartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class WheelService {

    @Autowired
    private WheelRepository wheelRepository;

    @Autowired
    private WheelPartRepository wheelPartRepository;

    public Wheel getUsersWheelById(Long id, User user) {
        return wheelRepository.findByIdAndUserId(id, user.getId()).orElseThrow(
                () -> new ResourceNotFoundException(String.format("Wheel with id %d was not found", id))
        );
    }

    public Wheel saveWheel(Wheel wheel, User user) {
        wheel.setParts(prepareParts(wheel.getParts()));
        wheel.getParts().stream().forEach(part -> part.setWheel(wheel));
        wheel.setUser(user);
        return wheelRepository.save(wheel);
    }

    public List<Wheel> getWheels(User user) {
        return wheelRepository.findAll();
    }

    public Wheel replaceUsersWheel(Long wheelId, Wheel newWheel, User user) {
        Wheel wheel = wheelRepository.findByIdAndUserId(wheelId, user.getId()).orElseThrow(
                () -> new ResourceNotFoundException(String.format("Wheel with id %d for user with id %d was not found", wheelId, user.getId()))
        );
        wheel.setName(newWheel.getName());
        wheelPartRepository.deleteAllByWheelId(wheelId);
        newWheel.setParts(prepareParts(newWheel.getParts()));
        newWheel.getParts().stream().forEach(part -> part.setWheel(wheel));
        wheel.setParts(newWheel.getParts());
        return wheelRepository.save(wheel);
    }

    @Transactional
    public void deleteUsersWheel(Long wheelId, User user) {
        wheelRepository.deleteByIdAndUserId(wheelId, user.getId());
    }

    private List<WheelPart> prepareParts(List<WheelPart> parts) throws CustomException {
        parts.stream().forEach(part -> part.setProbability((float) Math.round(part.getProbability() * 100) / 100));
        Double sumOfProbabilities = parts.stream().mapToDouble(part -> part.getProbability()).sum();
        if (sumOfProbabilities != 1) {
            throw new CustomException(String.format("Sum of probabilities must be 1, %f given", sumOfProbabilities));
        }
        return parts;
    }
}
