package cz.titanus.wheeloffortuneapi.wheel;

import cz.titanus.wheeloffortuneapi.exceptions.ExceptionResponse;
import cz.titanus.wheeloffortuneapi.user.SimpleUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/wheels")
public class WheelController {

    @Autowired
    private WheelService wheelService;

    @GetMapping
    public List<Wheel> getWheels(@AuthenticationPrincipal SimpleUserDetails userDetails) {
        return wheelService.getWheels(userDetails.getUser());
    }

    @GetMapping("/{id}")
    public Wheel getWheel(@PathVariable Long id, @AuthenticationPrincipal SimpleUserDetails userDetails) {
        return wheelService.getUsersWheelById(id, userDetails.getUser());
    }

    @PostMapping
    public Wheel saveWheel(@RequestBody Wheel wheel, @AuthenticationPrincipal SimpleUserDetails userDetails) {
        return wheelService.saveWheel(wheel, userDetails.getUser());
    }

    @PutMapping("/{id}")
    public Wheel replaceWheel(@PathVariable Long id, @RequestBody Wheel wheel, @AuthenticationPrincipal SimpleUserDetails userDetails) {
        return wheelService.replaceUsersWheel(id, wheel, userDetails.getUser());
    }

    @DeleteMapping("/{id}")
    public void deleteWheel(@PathVariable Long id, @AuthenticationPrincipal SimpleUserDetails userDetails) {
        wheelService.deleteUsersWheel(id, userDetails.getUser());
    }
    
}
