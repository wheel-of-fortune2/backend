package cz.titanus.wheeloffortuneapi.jwt;

import cz.titanus.wheeloffortuneapi.exceptions.UnauthorizedException;
import cz.titanus.wheeloffortuneapi.user.SimpleUserDetails;
import cz.titanus.wheeloffortuneapi.user.User;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@Component
public class JwtAuthorizationFilter extends OncePerRequestFilter {
    @Autowired
    private JwtConfig jwtConfig;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String authHeader = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);
        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }
        try {
            String token = authHeader.replace("Bearer ", "");
            Jws<Claims> claims = Jwts.parserBuilder()
                    .setSigningKey(Keys.hmacShaKeyFor(jwtConfig.getSecretKey().getBytes()))
                    .build().parseClaimsJws(token);
            SimpleUserDetails userDetails = new SimpleUserDetails(new User(
                    Long.valueOf(claims.getBody().get("id").toString()),
                    claims.getBody().getSubject(),
                    null
            ));
            SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(
                    userDetails,
                    null,
                    new ArrayList<>()
            ));
        } catch (JwtException e) {
            throw new UnauthorizedException("Invalid token");
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
