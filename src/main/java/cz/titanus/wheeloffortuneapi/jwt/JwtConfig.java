package cz.titanus.wheeloffortuneapi.jwt;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "application.jwt")
public class JwtConfig {
    private String secretKey;
    private int expiration;

    public JwtConfig() {
    }

    public String getSecretKey() {
        return secretKey;
    }

    public int getExpiration() {
        return expiration;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public void setExpiration(int expiration) {
        this.expiration = expiration;
    }
}
