package cz.titanus.wheeloffortuneapi.wheelPart;

import com.fasterxml.jackson.annotation.JsonBackReference;
import cz.titanus.wheeloffortuneapi.wheel.Wheel;

import javax.persistence.*;

@Entity
@Table(name = "wheel_part")
public class WheelPart {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wheel_part_generator")
    @SequenceGenerator(name = "wheel_part_generator", sequenceName = "wheel_part_sequence", allocationSize = 50)
    private Long id;
    private float probability;
    @Column(length = 30)
    private String label;

    private int timesSpun;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "wheel_id")
    private Wheel wheel;

    protected WheelPart() {
    }

    public WheelPart(float probability, String label) {
        this.probability = probability;
        this.label = label;
    }

    public Long getId() {
        return id;
    }

    public float getProbability() {
        return probability;
    }

    public String getLabel() {
        return label;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setProbability(float probability) {
        this.probability = probability;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Wheel getWheel() {
        return wheel;
    }

    public int getTimesSpun() {
        return timesSpun;
    }

    public void setTimesSpun(int timesSpun) {
        this.timesSpun = timesSpun;
    }

    public void setWheel(Wheel wheel) {
        this.wheel = wheel;
    }
}
