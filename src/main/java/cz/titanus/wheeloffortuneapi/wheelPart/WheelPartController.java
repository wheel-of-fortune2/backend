package cz.titanus.wheeloffortuneapi.wheelPart;

import cz.titanus.wheeloffortuneapi.user.SimpleUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/wheel-parts")
public class WheelPartController {

    @Autowired
    private WheelPartService wheelPartService;

    @PatchMapping("/{id}")
    public WheelPart setTimesSpun(@PathVariable Long id, @RequestBody WheelPart wheelPart, @AuthenticationPrincipal SimpleUserDetails userDetails) {
        return wheelPartService.setTimesSpunOfUsersWheelPart(id, wheelPart.getTimesSpun(), userDetails.getUser().getId());
    }
}
