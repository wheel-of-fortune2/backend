package cz.titanus.wheeloffortuneapi.wheelPart;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WheelPartRepository extends JpaRepository<WheelPart, Long> {
    Long deleteAllByWheelId(Long wheelId);
}
