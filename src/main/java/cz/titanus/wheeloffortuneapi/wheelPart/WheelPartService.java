package cz.titanus.wheeloffortuneapi.wheelPart;

import cz.titanus.wheeloffortuneapi.exceptions.ResourceNotFoundException;
import cz.titanus.wheeloffortuneapi.exceptions.UnauthorizedException;
import cz.titanus.wheeloffortuneapi.wheel.Wheel;
import cz.titanus.wheeloffortuneapi.wheel.WheelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WheelPartService {

    @Autowired
    private WheelPartRepository wheelPartRepository;

    public WheelPart setTimesSpunOfUsersWheelPart(Long id, int timesSpun, Long userId) {
        WheelPart part = wheelPartRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException(String.format("Wheel part with id %d was not found", id))
        );
        if (part.getWheel().getUser().getId() != userId) {
            throw new UnauthorizedException(String.format("Wheel part with id %d belongs to a different user", id));
        }
        part.setTimesSpun(timesSpun);
        return wheelPartRepository.save(part);
    }
}
