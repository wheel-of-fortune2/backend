package cz.titanus.wheeloffortuneapi;

import cz.titanus.wheeloffortuneapi.user.User;
import cz.titanus.wheeloffortuneapi.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CmdRunner implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        userRepository.saveAll(List.of(
                new User("martin", passwordEncoder.encode("password")),
                new User("user", passwordEncoder.encode("user"))
        ));
    }
}
