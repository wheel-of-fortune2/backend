package cz.titanus.wheeloffortuneapi;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class WheelOfFortuneApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(WheelOfFortuneApiApplication.class, args);
	}

}
