# Wheel of Fortune - backend
App for customizable Wheels of Fortune. The goal was to learn typescript along with Spring Boot. Sadly, I had to leave this project unfinished, because of needed preparation for my final exams (maturita).

## Features
- REST API, MVC, ORM (Data JPA)
- Use of request filters
- Fine exception handling
- Authentication with JWT

## Used Technologies
- [Spring Boot](https://spring.io/projects/spring-boot)
- [jjwt](https://github.com/jwtk/jjwt) - JWT Java library
